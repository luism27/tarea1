<?php
    include_once ("Pagina.php");
    include_once ("Routes.php");
    include_once ("Storage.php");
    $str=new Storage();
    
    $pag= new Pagina("page_1","page_1.php");
    $pago= new Pagina("page_2","page_2.php");
    $ani=array($pag,$pago);
    $rou = new Routes("More",true,$ani);
    $rous = new Routes("Most",false,$ani);
    $str->add($rou);
    $str->add($rous);
    
  
 
    
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <!--??, significa que si el title no esta definida use el string "Page"-->
  <title><?= $title ?? 'Page' ?></title>
  <link rel="stylesheet" type="text/css" href="./css/bulma.min.css">
  <link rel="stylesheet" type="text/css" href="./css/style.css">
  <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
<nav class="navbar" role="navigation" aria-label="main navigation">
  <div class="navbar-brand">
    <a class="navbar-item" href="/">
      <img src="./imgs/logo.png" >
    </a>

    <a role="button" class="navbar-burger burger" aria-label="menu" aria-expanded="false" data-target="navbarBasicExample">
      <span aria-hidden="true"></span>
      <span aria-hidden="true"></span>
      <span aria-hidden="true"></span>
    </a>
  </div>

  <div id="navbarBasicExample" class="navbar-menu">
    <div class="navbar-start">
      <?php
          
             foreach ($str->getArray() as $view) {
              if(!$view->anidado){
                foreach ($view->paginas as $page) {
                  
                  ?>
      <a class="navbar-item" href="<?=$page->url?>">
        <?=$page->name?>
      </a>
      <?php 
          }
        }else{
       ?>
         <div class="navbar-item has-dropdown is-hoverable">
        <a class="navbar-link">
          <?= $view->name?>
        </a>
        <div class="navbar-dropdown">
          <?php 
            foreach ($view->paginas as $pan) {?>
              
              <a class="navbar-item" href="<?=$pan->url?>">
                <?=$pan->name?>
               </a>
              <?php 
                }
               ?>
            </div>
           </div>
     <?php }

      } ?>


        
      
    </div>
  </div>


    <div class="navbar-end">
      <div class="navbar-item">
        <div class="buttons">
          <a class="button is-primary">
            <strong>Sign up</strong>
          </a>
          <a class="button is-light">
            Log in
          </a>
        </div>
      </div>
    </div>
  </div>
</nav>